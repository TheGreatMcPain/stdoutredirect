# stdoutRedirect

This is a class that redirects stdout to buffer.

Example program exists in `main.cpp`.

The code was adapted from this [stackoverflow answer.](https://stackoverflow.com/a/956269/13793328)

## Usage

| Method | Description |
|:---|:---|
| stdoutRedirect.StartRedirect() | Starts redirecting stdout to a buffer. |
| stdoutRedirect.StopRedirect() | Stops redirecting stdout. |
| stdoutRedirect.GetBuffer() | Returns buffer as std::string. |
| stdoutRedirect.ClearBuffer() | Resets buffer. |
