/*
 * Made By James McClain
 *
 * This code is in the Public Domain
 */
#pragma once

#include <fcntl.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <sstream>
#include <thread>

#if defined(_WIN32)
#include <io.h>
#define read _read
#define open _open
#define dup _dup
#define dup2 _dup2
#endif

#define BUFFERSIZE 1024

class stdoutRedirect {
 public:
  inline void StartRedirect();
  inline void StopRedirect();
  inline void ClearBuffer();
  inline std::string GetBuffer();
  inline stdoutRedirect();
  inline ~stdoutRedirect();

  class Exception : public std::exception {
   public:
    explicit Exception(std::string message) : m_message(std::move(message)) {}
    const char *what() const noexcept override { return m_message.c_str(); }

   private:
    std::string m_message;
  };

 private:
  inline void BufferThread();
  int mOutPipe[2];
  int mSavedStdout;
#if defined(_WIN32)
  FILE *mSavedStdoutPtr;
#endif
  bool mStoppedRedirect;
  bool mThreadRunning;
  std::thread *mBufferThread;
  std::stringstream mBuffer;
};

stdoutRedirect::stdoutRedirect() {
  // save stdout for later
  if ((mSavedStdout = dup(STDOUT_FILENO)) == -1) {
    std::string ErrorMessage = "Failed to save stdout.";
    throw stdoutRedirect::Exception(ErrorMessage);
  }

#if defined(_WIN32)
  // Ran into an issue where stdout would close
  // after a library was done, but this only happened on windows.
  // Will be used to restore stdout. (Only on Windows)
  mSavedStdoutPtr = fdopen(mSavedStdout, "w");
#endif
  mStoppedRedirect = false;
  mThreadRunning = false;
}

stdoutRedirect::~stdoutRedirect() {
  if (!mStoppedRedirect) {
    StopRedirect();
  }
}

// Starts redirecting stdout, and start bufferThread.
void stdoutRedirect::StartRedirect() {
  // Create pipe
#if !defined(_WIN32)
  if (pipe(mOutPipe) != 0) {
#else
  if (_pipe(mOutPipe, BUFFERSIZE, O_BINARY) != 0) {
#endif
    std::string ErrorMessage = "Failed to create pipe.";
    throw stdoutRedirect::Exception(ErrorMessage);
  }

  // Doesn't seem to make a difference if this is here.
  setvbuf(stdout, NULL, _IONBF, 0);

  // redirect stddout to the pipe
  dup2(mOutPipe[1], STDOUT_FILENO);
  fflush(stdout);
  close(mOutPipe[1]);

  // Start sending the read end of the stdout pipe to mBuffer.
  mBufferThread = new std::thread(&stdoutRedirect::BufferThread, this);
  mThreadRunning = true;
}

// Attempts to restore stdout.
void stdoutRedirect::StopRedirect() {
  // Reconnect stdout
  if (-1 == dup2(mSavedStdout, STDOUT_FILENO)) {
    std::string ErrorMessage = "Can't replace stdout";
    throw stdoutRedirect::Exception(ErrorMessage);
  }

#if defined(_WIN32)
  // If stdout happens to be closed lets restore with our FILE ptr.
  // (Only Windows)
  *stdout = *mSavedStdoutPtr;
#endif

  if (mThreadRunning) {
    // Wait for bufferThread to stop.
    mBufferThread->join();
    // Delete thread.
    delete mBufferThread;
  }
  mStoppedRedirect = true;
}

// Thread function that writes to mBuffer from pipe.
void stdoutRedirect::BufferThread() {
  char cbuffer[BUFFERSIZE];
  std::memset(cbuffer, 0, BUFFERSIZE);
  while (read(mOutPipe[0], cbuffer, BUFFERSIZE) > 0) {
    mBuffer << cbuffer;
    std::memset(cbuffer, 0, BUFFERSIZE);
  }
}

// Returns mBuffer as std::string.
std::string stdoutRedirect::GetBuffer() {
  if (mStoppedRedirect) {
    return mBuffer.str();
  } else {
    std::string ErrorMessage =
        "Please call StopRedirect() before calling GetBuffer()";
    throw stdoutRedirect::Exception(ErrorMessage);
  }
}

// Resets mBuffer.
void stdoutRedirect::ClearBuffer() {
  if (mStoppedRedirect) {
    // Empty mBuffer, and clear any possible errors.
    mBuffer.str(std::string());
    mBuffer.clear();
  } else {
    std::string ErrorMessage =
        "Please call StopRedirect() before calling ClearBuffer()";
    throw stdoutRedirect::Exception(ErrorMessage);
  }
}
