#include <iostream>

#include "stdoutredirect.hpp"

#define NUMLINES 1000

int main() {
  stdoutRedirect stdoutReTest;

  std::cout << "Testing redirect." << std::endl;

  // Redirect stdout to buffer.
  stdoutReTest.StartRedirect();
  // Dump a bunch of lines to stdout/buffer.
  for (int x = 0; x < NUMLINES; x++) {
    std::cout << "You can't see me." << std::endl;
    // Send status to stderr.
    std::cerr << "\r" << (x + 1) << " out of " << NUMLINES
              << " lines written to stdout/buffer." << std::flush;
  }
  std::cerr << std::endl;

  // Restore stdout.
  stdoutReTest.StopRedirect();

  // Return contents of buffer to stdout.
  std::cout << stdoutReTest.GetBuffer() << std::endl;

  // Reset stdoutRedirect's buffer.
  stdoutReTest.ClearBuffer();

  return 0;
}

// Lets test reading directly from the buffer.
